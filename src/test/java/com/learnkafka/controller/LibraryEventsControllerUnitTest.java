package com.learnkafka.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learnkafka.domain.Book;
import com.learnkafka.domain.LibraryEvent;
import com.learnkafka.service.LibraryEventProducer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(LibraryEventsController.class)
@AutoConfigureMockMvc
public class LibraryEventsControllerUnitTest {

    private static final String LIBRARY_EVENT_API = "/v1/libraryevent";

    @Autowired
    MockMvc mvc;

    @MockBean
    LibraryEventProducer libraryEventProducer;

    private Book book;
    private LibraryEvent libraryEvent;
    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void givenLibraryEventWhenPostToKafkaThenOk() throws Exception {
        book = Book.builder()
                .id(1)
                .bookName("Kafka using Spring boot")
                .bookAuthor("Tofik")
                .build();

        libraryEvent = LibraryEvent.builder()
                .libraryEventId(23)
                .book(book)
                .build();

        String json = mapper.writeValueAsString(libraryEvent);
        when(libraryEventProducer.sendLibraryEvent_2(isA(LibraryEvent.class))).thenReturn(null);

        mvc.perform(post(LIBRARY_EVENT_API)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

    }

    @Test
    public void givenLibraryEventWhenPostToKafkaThenBadRequest() throws Exception {
        book = Book.builder()
                .id(null)
                .bookName("Kafka using Spring boot")
                .bookAuthor(null)
                .build();

        libraryEvent = LibraryEvent.builder()
                .libraryEventId(23)
                .book(book)
                .build();

        String json = mapper.writeValueAsString(libraryEvent);

        when(libraryEventProducer.sendLibraryEvent_2(isA(LibraryEvent.class))).thenReturn(null);

        String expectedErrorMessage = "book.bookAuthor - must not be blank, book.id - must not be null";

        mvc.perform(post(LIBRARY_EVENT_API)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
        .andExpect(content().string(expectedErrorMessage));
    }

    @Test
    public void givenLibraryEventWhenPutToKafkaThenOk() throws Exception {
        book = Book.builder()
                .id(1)
                .bookName("Kafka using Spring boot")
                .bookAuthor("Tofik")
                .build();

        libraryEvent = LibraryEvent.builder()
                .libraryEventId(23)
                .book(book)
                .build();

        String json = mapper.writeValueAsString(libraryEvent);
        when(libraryEventProducer.sendLibraryEvent_2(isA(LibraryEvent.class))).thenReturn(null);

        mvc.perform(put(LIBRARY_EVENT_API)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void givenLibraryEventWhenPutToKafkaThenBadRequest() throws Exception {
        book = Book.builder()
                .id(1)
                .bookName("Kafka using Spring boot")
                .bookAuthor("Tofik")
                .build();

        libraryEvent = LibraryEvent.builder()
                .libraryEventId(null)
                .book(book)
                .build();

        String json = mapper.writeValueAsString(libraryEvent);

        when(libraryEventProducer.sendLibraryEvent_2(isA(LibraryEvent.class))).thenReturn(null);

        String expectedErrorMessage = "Please pass the LibraryEventId";

        mvc.perform(put(LIBRARY_EVENT_API)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(expectedErrorMessage));
    }

}
